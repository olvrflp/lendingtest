
class Service(object):
    def __init__(self):
        self.threshold = 50000

    def getDecision(self, requested_ammount):
        decision = "rejected"
        if requested_ammount < self.threshold:
            decision = "approved"
        elif requested_ammount == self.threshold:
            decision = "Undecided"
        return { "decision": decision }
