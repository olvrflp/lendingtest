import tornado.ioloop
import tornado.web
import json
from service import Service
from tornado import httputil
from typing import Any


class MainHandler(tornado.web.RequestHandler):

    def __init__(
        self,
        application: "Application",
        request: httputil.HTTPServerRequest,
        **kwargs: Any
    ):
        super().__init__(application, request, **kwargs)
        self.service = Service()

    def get(self):
        self.write("Hello, world")

    def post(self):
        requested_ammount = int(self.get_body_argument("requested_ammount"))

        self.write(json.dumps(self.service.getDecision(requested_ammount)))

    def prepare(self):
        if self.request.headers.get("Content-Type", "").startswith("application/json"):
            self.json_args = json.loads(self.request.body)
        else:
            self.json_args = None
            
    def set_default_headers(self):
        self.set_header('Content-Type', 'application/json')


def make_app():
    return tornado.web.Application([
        (r"/", MainHandler),
    ])


if __name__ == "__main__":
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
