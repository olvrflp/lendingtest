import React from 'react';
import { AppProvider } from './components/AppContext';
import Routes from './components/Routes';

export const App = () => {
    return (
        <div className="container">
            <Routes />
        </div>
    );
}
