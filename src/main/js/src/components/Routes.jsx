import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Decision } from './Decision';
import LendRequestForm from './LendRequestForm';

const Routes = () => (
    <Router>
        <Route exact path="/" component={LendRequestForm} />
        <Route path="/decision" component={Decision} />
    </Router>
);

export default Routes;