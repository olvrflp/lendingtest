import { useContext } from "react";
import { Field, Form } from "react-final-form";
import { withRouter } from "react-router-dom";
import AppContext, { AppProvider } from "./AppContext";

const RequestForm = (props) => {

    const context = useContext(AppContext);

    const onSubmit = (values) => {
        console.log(values);
        context.decision = "approved";
        props.history.push('/decision');
    }

    return (
        <Form
            onSubmit={onSubmit}
            render={({ handleSubmit }) => (
                <form onSubmit={handleSubmit} >
                    <div className="form-group">
                        <label>Tax ID: </label>
                        <Field name="taxId" className="form-control" component="input" />
                    </div>
                    <div className="form-group">
                        <label>Business Name: </label>
                        <Field name="businessName" className="form-control" component="input" />
                    </div>
                    <div className="form-group">
                        <label>Requested amount: </label>
                        <Field name="requestedAmmount" className="form-control" component="input" />
                    </div>
                    <div className="form-group">
                        <label>Owner Social Security Number: </label>
                        <Field name="ownerSSN" className="form-control" component="input" />
                    </div>
                    <div className="form-group">
                        <label>Owner Name: </label>
                        <Field name="ownerName" className="form-control" component="input" />
                    </div>
                    <div className="form-group">
                        <label>Owner email: </label>
                        <Field name="ownerEmail" type="email" className="form-control" component="input" />
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            )}
        />
    );
}

const LendRequestForm = () => {
    return (
        <AppProvider value={{}}>
            <RequestForm />
        </AppProvider>
    );
}

export default withRouter(LendRequestForm);
