import { useContext } from "react";
import AppContext, { AppProvider, decision } from "./AppContext";

const DecisionPage = () => {
    const context = useContext(AppContext);
    console.log(context);

    return (
        <div className="alert alert-success"> Approved </div>
    );
}

export const Decision = () => {

    return (
        <AppProvider value={{ decision }} >
            <DecisionPage />
        </AppProvider>
    );
}
