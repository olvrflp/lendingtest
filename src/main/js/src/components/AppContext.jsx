import React, { useReducer } from 'react'

const decisionReducer = (state, item) => {
    return { ...state, item };
}

export const [ decision, setDecision ] = useReducer(decisionReducer, { decision: 'Approved' });

const AppContext = React.createContext([{ decision: 'Approved' }, (values) => ({ ...values })]);
export const AppProvider = AppContext.Provider;

export default AppContext;
